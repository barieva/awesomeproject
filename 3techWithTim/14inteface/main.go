package main

import "fmt"

// Interface which have method
type carStore interface {
	calculateCarPrice(configuration string) int64
}

// One struct can implement several interfaces
type building interface {
	calculateBuildingPrice(year uint64) int64
}
type lightCarStore struct {
	name string
}

type truckStore struct {
	name string
}

// We can pass pointer to method. And instance need to be a pointer when
// we want it to be in a slice of interface type
func (lightCar *lightCarStore) calculateCarPrice(configuration string) int64 {
	if configuration == "Basic" {
		return 51213
	}
	if configuration == "Premium" {
		return 91223
	}
	return 64231
}

func (truck truckStore) calculateCarPrice(configuration string) int64 {
	if configuration == "Commercial" {
		return 31213
	}
	if configuration == "Premium" {
		return 21223
	}
	return 14231
}

func (store truckStore) calculateBuildingPrice(year uint64) int64 {
	if year == 2021 {
		return 512312
	}
	return 412311
}

func getPremiumCarPrice(carStore carStore) int64 {
	return carStore.calculateCarPrice("Premium")
}

func main() {
	lightCarStore := lightCarStore{"MERCEDES-BENZ"}
	truckStore := truckStore{"MERCEDES-BENZ"}
	var carStore = []carStore{truckStore, &lightCarStore}
	var buildings = []building{truckStore}
	for _, store := range carStore {
		price := getPremiumCarPrice(store)
		fmt.Println("FIRST INTERFACE:", price)
	}
	for _, store := range buildings {
		fmt.Println("SECOND INTERFACE:", store.calculateBuildingPrice(2021))
	}
}
