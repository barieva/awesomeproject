package main

import "fmt"

func main() {
	var int64Num int64 = 10
	var int64Num2 int64 = 10
	sameTypeResult := int64Num + int64Num2 // arithmetic with same types
	fmt.Printf("%T\n", sameTypeResult)
	var intNum int = 3
	var float64Num float64 = 3.3
	castingTypeResult := float64Num + float64(intNum) // arithmetic with different types
	fmt.Printf("%f\n", castingTypeResult)
	//differentTypeResult := int64Num + intNum // arithmetic with different types

	fmt.Println(int(float64Num)) // make float as int

	var nu float32 = 9
	var nu2 float32 = 4
	re := nu / nu2

	re2 := 1 % 23
	fmt.Println(re2)
	fmt.Printf("%g\n", re)

	x := 5
	val := x == 5
	fmt.Printf("%t", val)

	fmt.Println("We can compare characters and strings by <, >, <-,"+
		"and == signs ", 'f' > 'B')

	val1 := true && false || true
	fmt.Println(val1)
}
