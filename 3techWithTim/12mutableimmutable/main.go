package main

import "fmt"

func main() {
	//primitive
	var price int = 50023
	independentCopyOfPrice := price
	independentCopyOfPrice = 12300
	fmt.Println(price)
	fmt.Println(independentCopyOfPrice)
	// array
	var prices [5]int = [5]int{23023, 44241, 74500}
	var independentCopyOfPrices = prices
	independentCopyOfPrices[0] = 0
	fmt.Println(prices)
	fmt.Println(independentCopyOfPrices)
	// slice
	var slicePrices []int = []int{13023, 94241, 104500}
	var dependentSlicePrices = slicePrices
	dependentSlicePrices[0] = 777
	fmt.Println(slicePrices)
	fmt.Println(dependentSlicePrices)
	//	map
	var mapCarByPrices map[string]uint64 = map[string]uint64{
		"Mercedes-Benz": 123123,
		"Audi":          73123,
		"BMW":           93123,
	}
	var dependentCopyOfmapCarByPrices = mapCarByPrices
	dependentCopyOfmapCarByPrices["BMW"] = 55555
	fmt.Println(mapCarByPrices)
	fmt.Println(dependentCopyOfmapCarByPrices)

	//slice with method example
	var priceSlice []int = []int{12000, 34123, 51234}
	//put real object
	changeFirst(priceSlice)
	fmt.Println(priceSlice)
}

func changeFirst(price []int) {
	price[0] = 1000
}
