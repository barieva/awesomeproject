package main

import "fmt"

func main() {
	var prices [5]int = [5]int{10000, 20000, 40000}

	// we will declare array without capacity.
	//  it is SLICE.   [] <- no capacity
	//var pricesSlice []int
	var pricesSlice []int = prices[:2]
	fmt.Println(pricesSlice)
	fmt.Println("LEN IS", len(pricesSlice)) // NUMBER OF ELEMENTS IN OUR CASE 2
	fmt.Println("CAP IS", cap(pricesSlice)) // IS WHOLE CAPACITY OF ARRAY IN OUR CASE EQUALS TO 5

	var a []int = []int{5, 6, 7, 8, 9}
	b := append(a, 123)
	fmt.Println(b)

	concurrentWay := make([]string, 5)
	fmt.Printf("%T", concurrentWay)
}
