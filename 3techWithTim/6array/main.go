package main

import (
	"fmt"
	"sort"
	"strings"
	"time"
)

type ByDate []time.Time

func (a ByDate) Len() int           { return len(a) }
func (a ByDate) Less(i, j int) bool { return a[i].Before(a[j]) }
func (a ByDate) Swap(i, j int)      { a[i], a[j] = a[j], a[i] }

type a struct {
	f string
}

func main() {

	var a = time.Time{}

	s1 := &a

	if s1.IsZero() {
		fmt.Println("HELLOWORLD")
	}
	m := map[string][]string{}
	var v []string
	m["a"] = v

	s := m["a"]

	if s == nil {
		fmt.Println("SO GOOD")
	} else {
		s2 := s[len(s)-1]
		fmt.Println(s2)
	}

	var f time.Time
	zero := time.Time{}.IsZero()
	if zero {
		fmt.Println("EEEEE")
	}

	as := []time.Time{f}
	sort.Sort(ByDate(as))
	fmt.Println(as)
	fmt.Println("Sd")

	arr := [5]int{4, 1, 2, 3, 4}
	fmt.Println(arr)
	var sum int
	for i := 0; i < len(arr); i++ {
		sum += arr[i]
	}
	fmt.Println(sum)

	var array2d = [2][2]int{{0, 0}, {1, 2}}
	fmt.Println(array2d[1][0])
	println(strings.HasPrefix("MYMALLa", "MYMALL"))

}
