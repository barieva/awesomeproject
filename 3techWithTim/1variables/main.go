package main

import "fmt"

func main() {
	fmt.Println("Hello word")
	//var name string = string("HELLO BABY!")
	//fmt.Println(name)

	//var _ uint16 = 256
	//var _ uint16 = 256
	//var _ uint16 = 256
	//var _ uint16 = 256
	//var _ uint16 = 256
	var name uint16 = (5)
	fmt.Println(name)
	name = name + 5
	fmt.Println(name)

}

//  integer type categories

//unsigned
//uint8 / byte - 0 - 255
//uint16 0 - 2^16 -1
//uint32 0 - 2^32 -1
//uint64 0 - 2^64 -1

//signed
//int8 - -2^7 - 2^7 - 1
//int16 - -2^15 - 2^15 -1
//int32 /rune  - -2^31 - 2^31 -1
//int64 - -2^63 - 2^63 -1
