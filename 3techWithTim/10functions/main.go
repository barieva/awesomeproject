package main

import "fmt"

func main() {
	turn, on := turnOn(5, 1)
	fmt.Println(turn)
	fmt.Println(on)
}
func turnOn(x, y int) (first int, second int) {
	defer fmt.Println("defEr - execute later")
	first = x + y
	second = 1
	fmt.Println("Hello bone")
	return
}
