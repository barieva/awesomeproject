package main

import "fmt"

type Car struct {
	name   string
	price  uint64
	colors []int64
}

func (car *Car) setCarName(name string) {
	car.name = name
}

func (car Car) getAverageColor() float64 {
	var colorSum int64
	for _, color := range car.colors {
		colorSum += color
	}
	return float64(colorSum) / float64(len(car.colors))
}

func (car Car) getMaxColor() int64 {
	var maxColor int64
	for _, color := range car.colors {
		if maxColor < color {
			maxColor = color
		}
	}
	return maxColor
}
func main() {
	car := Car{"Mercedes-Benz", 51231, []int64{134, 5, 234, 123, 5, 12}}
	car2 := Car{"Mercedes-Benz", 51231, []int64{134, 5, 231234, 123, 51231, 12}}
	fmt.Println(car)
	car.setCarName("Bentley")
	fmt.Println(car)
	fmt.Println(car.getAverageColor())
	fmt.Println(car2.getAverageColor())
	fmt.Println(car.getMaxColor())
	fmt.Println(car2.getMaxColor())
}
