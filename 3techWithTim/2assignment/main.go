package main

import "fmt"

func main() {
	var nu uint64
	var bl bool

	fmt.Println(nu, bl)

	fmt.Printf("Hello %T %v %s", 10, 10, string(10))
	fmt.Printf("Hello  %t\n", true)
	fmt.Printf("Hello  %b\n", 1024)                   // byte
	fmt.Printf("Hello  %x\n", 16)                     // ?
	fmt.Printf("Hello %.2f\n", 3.42123)               //round to TWO characters after dot
	fmt.Printf("Hello %f\n", 3.42123)                 //round to NO characters after dot
	fmt.Printf("Hello %15s\n", "gime quotes")         //quotes around string
	fmt.Printf("Hello %15q\n", "gime quotes")         // does padding
	var out = fmt.Sprintf("Number: %09d is cool", 45) // assigns printF result to variable as string
	fmt.Println(out)

	//fmt.Sprintf("Hello %t %v %", 10, 10)
}
