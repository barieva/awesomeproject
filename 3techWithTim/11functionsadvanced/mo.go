package main

import "fmt"

func main1() {
	bossNameFunctionVariable := func(boss string) string {
		return boss + " in Maserati"
	}("BOOOSSS")
	fmt.Println(bossNameFunctionVariable)

	f := func(price int64) string {
		if price < 60000 {
			return "Audi"
		}
		return "Mercedes-Benz"
	}

	gimeafuction(f)

	f2 := returnFunc("BBBBOOOOSS!!!")
	fmt.Println(f2("BONE"))
	fmt.Println(f2("BONE2"))
}

func gimeafuction(a func(price int64) string) {
	fmt.Println("your car ISSS!!!", a(62312))
}

func bossName() {
	fmt.Println("Boss in bentley")
}

func returnFunc(carname string) func(a string) string {
	i := 0
	return func(bone string) string {
		i++
		return fmt.Sprintln(bone, carname, i)
	}
}
