package main

import "fmt"

func main() {
	turnOn()           // 1) call function
	variable := turnOn // 2) assign function to variable
	variable()         // call function-variable

	// 3) We can even create function here and assign it to a variable
	// The function called as anonymous function
	variable2 := func(name string) {
		fmt.Println(name)
	}
	variable2("Function as parameter")

	// 4) we can call function right after declaration
	functionResult := func(number int) int {
		return number + 1
	}(10)
	fmt.Println("Result of declared func", functionResult)

	// 5) now we create function, assign it to variable,
	getPrice := func(carName string) int {
		if carName == "Mercedes-Benz" {
			return 75342
		}
		return 51233
	}
	// 6) pass the variable-function inside another function
	// which takes function as parameter
	takesFunc(getPrice)

	// 7) closure. read explanation below
	function := closureFunction()
	function()
	function()
	fmt.Println("CLOSURE \n i was ZERO but now is:", function())

}

func turnOn() {
	fmt.Println("Varible function")
}

// 6) we can even pass function inside of another fuction
// In java we need to create functional interface with one method for that purposes
// But in go we simply pass a method which match with signature

func takesFunc(paramterFunc func(carName string) int) {
	fmt.Println("Mercedes price is", paramterFunc("Mercedes-Benz"))
}

// 7) Here is simple closure function
//    Closure - just function which returns another function
//    We can get variable of closure function which will hold returned function
//    This variable will have memory of variables values inside closure
//    We can see it if we will simply create function. call it in code several times
//    i (variable) will be incremented several times
func closureFunction() func() int {
	i := 0
	return func() int {
		i++
		return i
	}
}
