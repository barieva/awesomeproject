package main

import "fmt"

func main() {
	var carsByPrices map[string]uint64 = map[string]uint64{
		"bmw":  51231,
		"audi": 41231,
	}
	fmt.Println(carsByPrices)

	//concurentSafe := make(map[string]uint64)
	carsByPrices["Meredes-Benz"] = 75000
	println(carsByPrices["Meredes-Benz"])
	println(carsByPrices["bmw1"])

	//	DELETE from MAP
	delete(carsByPrices, "bmw")
	fmt.Println(carsByPrices)

	//	GET VALUE FROM MAP
	value, exists := carsByPrices["audi"]
	if exists { // good for ZERO value. Decide it is really zero or default value
		fmt.Println("VALUE EXISTS BABY", value)
	}

	fmt.Println(len(carsByPrices))
}
