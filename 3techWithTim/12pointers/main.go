package main

import "fmt"

func main() {
	x := 7
	y := &x // pointer to -> x
	fmt.Println(x, y)
	*y = 0b0111
	fmt.Println(x, y) // we do not change the pointer. it will stay the same

	// func with changing string value
	var bentley = "BENTLEY"
	// can change the value, because we pass pointer
	changeCarName(&bentley)
	// cant change the value, because we pass value
	dontChangeCarName(bentley)
	fmt.Println(bentley)

	var hello = "hello"
	var pointer *string = &hello
	fmt.Println(pointer, &pointer)
	var poninterOfPointer = &pointer
	fmt.Println(**poninterOfPointer)
}

func changeCarName(carName *string) {
	*carName = "Mercedes-Benz"
}

func dontChangeCarName(carName string) {
	carName = "Maserati"
}
