package main

import "fmt"

type car struct {
	name  string
	price int
}

func (c car) calculatePrice() {
	if c.name == "Mercedes-Benz" {
		c.price *= 2
	}
}

func (c *car) calculatePriceFromPointer() {
	if c.name == "Mercedes-Benz" {
		c.price *= 2
	}
}

func main() {
	c := car{"Mercedes-Benz", 10000}
	c.calculatePrice()
	fmt.Println(c.price)
	c.calculatePriceFromPointer()
	fmt.Println(c.price)
}
