package main

import "fmt"

func main() {
	me := recursiveMe(5)
	fmt.Println(me)
}

func recursiveMe(num int) int {
	if num == 1 {
		fmt.Println(num)
		return num
	}
	fmt.Println(num)
	return num * recursiveMe(num-1)
}
