package main

import "fmt"

func variadicInts(prices ...int) int {
	var totalPrice int
	for _, price := range prices {
		totalPrice += price
	}
	return totalPrice
}

func main() {
	//Println is an example of a variadic function
	fmt.Println("this", "is", "variadic", "function")

	//call function with infinite number of prices
	fmt.Println(variadicInts(10000, 20000, 30000, 40000, 50000))

	//variadic functions can also be used with SLICES:
	prices := []int{10, 10}
	// to pass slice in variadic function we need to add three dots after it (unpack it)
	fmt.Println(variadicInts(prices...))
}
