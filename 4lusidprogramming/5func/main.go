package main

import "fmt"

func add(a int, b int) int {
	return a + b
}

func main() {
	ans := add(1, 1)
	fmt.Println(ans)
	ans3 := add3(1, 1, 1)
	fmt.Println(ans3)

}

func add3(a, b, c int) int {
	return a + b + c
}
