package main

import (
	"fmt"
)

func main() {
	price := 20
	fmt.Println(price)
	pointerPrice := &price
	fmt.Println("POINTER", pointerPrice)
	fmt.Println("DEREFERENCE POINTER BABY", *pointerPrice)
	*pointerPrice = 10
	fmt.Println("CHANGED VARIABLE"+
		" VALUE THROUGH POINTER", *pointerPrice)
	fmt.Println("EVEN ORIGINAL VARIABLE"+
		"VALUE HAS CHANGED", price)
	price = 50
	fmt.Println("POINTER GOT NEW VALUE", *pointerPrice)
}
