package main

import "fmt"

func main() {
	m := make(map[string]int)
	m["a"] = 0
	m["b"] = 1

	fmt.Println(m)
	// print value of key
	fmt.Println(m["bsd"])
	//get length of map
	fmt.Println(len(m))
	//delete element from map
	delete(m, "a")
	fmt.Println(m)
	//initialize map and assign values
	m2 := map[string]int{"fam": 1, "bar": 532, "cof": 031}
	fmt.Println(m2)

	// the value and wheter the value is present.
	val, is_val_present := m["b"]
	fmt.Println(val)
	fmt.Println(is_val_present)
	_, is_val_present2 := m["a"]
	fmt.Println(is_val_present2)

}
