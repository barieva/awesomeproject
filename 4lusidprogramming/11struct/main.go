package main

import "fmt"

func main() {
	fmt.Println(car{name: "Mercedes-Benz", price: 51230})
	fmt.Println(car{"bmw", 51230})
	// didnt specify id and it have got default value
	fmt.Println(car{name: "bmw"})

	audi := car{name: "audi", price: 52304}
	fmt.Println(audi.name)
	fmt.Println(audi.price)
	pointerAudi := &audi
	// we can call value from struct pointer
	fmt.Println(pointerAudi.name)
	pointerAudi.name = "Maserati"
	fmt.Println(audi.name)
}

type car struct {
	name  string
	price int
}
