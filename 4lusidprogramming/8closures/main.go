package main

import (
	"fmt"
)

// 1) simple function. returns int
func getPrice(price int) int {
	return price * 2
}

// 3)same simple function. returns function
func getPriceFunc() func(int) {
	return func(price int) {
		fmt.Println(price * 2)
	}
}

// 4) closures - anonymous function which
//      references variables from outside its own body
func getClosureFunc() func() int {
	price := 10         // variable
	return func() int { // body
		price++ // was declared outside
		return price
	}
}

func main() {
	//1)simple outer function
	fmt.Println(getPrice(60))

	//2)anonymous function
	// as common function but without name
	price := func(price int) int {
		return price
	}(20) // called function right after declaration
	fmt.Println(price)

	//3)same simple function. returns function
	var priceFunc func(int) = getPriceFunc()
	priceFunc(50)

	//4) closure - simple function. returns anonymous function
	//             that anonymous function has reference to variable.
	//             variable is declared outside anonymous function body
	closureFunc := getClosureFunc()
	fmt.Println("Closure function", closureFunc)
	closurePrice := closureFunc()
	fmt.Println("Closure function variable", closurePrice)
	closurePrice2 := closureFunc()
	fmt.Println("Closure function variable keeps history of outside variable"+
		"and now price is higher:", closurePrice2)

	anotherClosureFunc := getClosureFunc()
	fmt.Println("New closure function", anotherClosureFunc)
	newClosurePrice := anotherClosureFunc()
	fmt.Println("New closure price", newClosurePrice)
	newClosurePrice2 := anotherClosureFunc()
	fmt.Println("New closure price second time", newClosurePrice2)

}
