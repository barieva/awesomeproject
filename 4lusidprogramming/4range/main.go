package main

import "fmt"

func main() {
	strings := []string{"a", "b", "c", "d"}
	for i, c := range strings {
		fmt.Println(i, c)
	}

	//range through key/value pairs
	m := map[string]int{
		"ba":  1,
		"abg": 2,
		"dba": 3,
		"baa": 4,
	}
	for k, v := range m {
		fmt.Println("Key is", k, "Value is", v)
	}
}
