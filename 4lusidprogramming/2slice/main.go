package main

import "fmt"

func main() {
	s := make([]int, 3)
	fmt.Println(s)
	s[0] = 1
	s[1] = 2
	s[2] = 3
	fmt.Println(s)
	fmt.Println(s[0])
	fmt.Println(len(s))
	// append function is unique to slices.
	s = append(s, 1, 9, 1) // here we can append several elements to our slice
	fmt.Println(s)
	fmt.Println(s[1:3])
	fmt.Println(s[:3])
	fmt.Println(s[1:])
	// concise (лаконичный) slice definition
	t := []int{100, 200, 300}
	/*	x := s
		fmt.Println(x)
		x[0] = 500 // here we changed slice and it changed for S variable
		fmt.Println(x, s)*/
	// if we have new slice from existent one
	fmt.Println(t)
	x := make([]int, len(s))
	copy(x, s) // first empty array, second array with elements
	x[0] = 500 // here we change slice element only for 'X' variable
	fmt.Println(x, s)

	// 2-D slice. Similar to arrays although lenghts of slice may vary.
	ss := make([][]int, 3)
	fmt.Println(ss)
	//[0][1,2][2,3,4]
	for i := 0; i < 3; i++ {
		inner_len := i + 1
		ss[i] = make([]int, inner_len)
		for j := 0; j < inner_len; j++ {
			ss[i][j] = i + j
		}
	}

}
