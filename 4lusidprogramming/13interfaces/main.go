package main

import "fmt"

// we want to create two interfaces with methods

type drivable interface {
	drive() int
	park()
}
type car struct {
	name   string
	horses int
}

type bicycle struct {
	speed int
}

func (c car) drive() int {
	fmt.Println("Exclusive Mercedes-Benz logic of car driving. Name is", c.name)
	return c.horses
}

func (b bicycle) drive() int {
	fmt.Println("Simple Stels logic of bicycle driving")
	return b.speed
}

func (c car) park() {
	fmt.Println("Parking premium Mercedes-Benz car")
}

func (b bicycle) park() {
	fmt.Println("Parking cheap Stels somewhere")
}

func start(vehicle drivable) {
	fmt.Println(vehicle)
	fmt.Println("speed is:", vehicle.drive())
	vehicle.park()
}

func main() {
	mercedes := car{name: "S-class coupe", horses: 531}
	stels := bicycle{10}
	start(mercedes)
	start(stels)

}
