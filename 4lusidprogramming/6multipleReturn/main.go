package main

import "fmt"

func main() {
	addition, subtraction := add_sub(1, 1)
	fmt.Println(addition, subtraction)

	_, subtraction2 := add_sub(1, 1)
	fmt.Println(subtraction2)
}

func add_sub(a, b int) (int, int) {
	return a + b, a - b
}
