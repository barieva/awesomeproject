package main

import "fmt"

type Car struct {
	ID    int
	name  string
	price int
	year  int
}

func (c Car) String() string {
	return fmt.Sprintf("Name:\t\t%q\n"+
		"Price:\t\t%v\nYear:\t\t%v\n", c.name, c.price, c.year)
}

var cars = []Car{
	Car{1, "BMW", 51303, 2021},
	Car{2, "Audi", 50303, 2021},
	Car{3, "Porsche", 61303, 2021},
	Car{4, "Mercedes-Benz", 56303, 2021},
	Car{5, "Maserati", 71303, 2021},
	Car{6, "Lexus", 61303, 2021},
	Car{7, "Lamborghini", 81303, 2021},
	Car{8, "Bentley", 1201303, 2021},
	Car{9, "Rolls Royce", 1101303, 2021},
}
