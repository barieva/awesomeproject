package main

import (
	"fmt"
	"math/rand"
	"sync"
	"time"
)

// SHOW ME RACE CONDITION WITH THE COMMAND
//go run --race .
var cache = make(map[int]Car)
var randomNumber = rand.New(rand.NewSource(time.Now().UnixNano()))

func main() {
	group := &sync.WaitGroup{} // waits for a bunch of GO-routines to finish
	mutex := &sync.RWMutex{}
	cacheCh := make(chan Car)
	dbCh := make(chan Car)
	for i := 0; i < 10; i++ {
		id := randomNumber.Intn(10) + 1
		group.Add(2)
		go func(id int, wg *sync.WaitGroup, mutex *sync.RWMutex, ch chan<- Car) {
			if car, ok := queryCache(id, mutex); ok {
				ch <- car
			}
			wg.Done()
		}(id, group, mutex, cacheCh)
		go func(id int, wg *sync.WaitGroup, mutex *sync.RWMutex, ch chan<- Car) {
			if car, ok := queryDatabase(id, mutex); ok {
				mutex.Lock()
				cache[id] = car
				mutex.Unlock()
				ch <- car
			}
			wg.Done()
		}(id, group, mutex, dbCh)
		//time.Sleep(150 * time.Millisecond)

		go func(cacheCh, dbCh <-chan Car) {
			select {
			case car := <-cacheCh:
				fmt.Println("from cache")
				fmt.Println(car)
				<-dbCh
			case car := <-dbCh:
				fmt.Println("from database")
				fmt.Println(car)
			}
		}(cacheCh, dbCh)
		time.Sleep(150 * time.Millisecond)
	}
	group.Wait() // wait before wg.Done() will be called twice

}

func queryCache(id int, mutex *sync.RWMutex) (Car, bool) {
	mutex.RLock()
	car, ok := cache[id]
	mutex.RUnlock()
	return car, ok
}

func queryDatabase(id int, mutex *sync.RWMutex) (Car, bool) { // RWMutex used when more readers than writers
	time.Sleep(100 * time.Millisecond)
	for _, car := range cars {
		if car.ID == id {
			return car, true
		}
	}
	return Car{}, false
}
