package main

import "fmt"

func main() {
	/*
		1 - Function, pointer receiving
		name := "Mercedes-Benz"
		for i := 0; i < 5; i++ {
			heyMercedes(i, &name)
		}
		fmt.Println(name)*/

	/*2 var args */
	/*	result := sum("Mercedes-Benz", 1, 2, 3, 4, 5)
		fmt.Println(*result)*/

	/*3 return function without name declaration in return */
	/*	carName := returnWithoutVariableName("Porsche")
		fmt.Println("Car name: ", carName)
	*/

	/*4 multiple return values*/
	d, err := divide(5.0, 0.0)
	if err != nil {
		fmt.Println("error here...")
		return
	}
	fmt.Println("it is good, boy", d)
}

/*
	1 - Function, pointer receiving

func heyMercedes(index int, name *string) {
	fmt.Println("How can i help you?")
	fmt.Printf("Your index is %v and name is %s", index, *name)
	*name = "BMW"
	fmt.Println(name)
}*/

/*
	2 var args

func sum(carName string, prices ...int) *int {
	fmt.Println(prices)
	fmt.Println(carName)
	// usually when we create variable inside the method
	// and the variable placed in STACK
	// after method ends -> variable destroyed
	stackResult := 1
	// but if we create the same variable in the method
	// but return it as pointer
	// then the variable will be placed inside the HEAP memory
	heapResult := 0
	for _, price := range prices {
		heapResult += price
	}
	fmt.Println(stackResult)
	return &heapResult
}
*/

/*
3 return function without name declaration in return

func returnWithoutVariableName(name string) (result string) {
	result = name
	return
}
*/

/* 4 multiple return values*/

func divide(a, b float64) (float64, error) {
	if b == 0 {
		return 0, fmt.Errorf("Cant devide by zero")
	}
	return a / b, nil
}
