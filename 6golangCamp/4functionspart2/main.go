package main

import "fmt"

func main() {

	/*
		  1	Concurrency issues when use
			for i := 0; i < 5; i++ {
				func() {
					fmt.Println(i)
				}()
			}
			time.Sleep(time.Second)*/

	/*
		Declare variable of function type
		var fu func() = func() {
			fmt.Println("Hello boss")
		}
		fu()
	*/
	var divide func(float64, float64) (float64, error)
	fmt.Println(divide)
	divide = func(a float64, b float64) (float64, error) {
		if b == 0 {
			return 0, fmt.Errorf("Devision by zero")
		}
		return a / b, nil
	}
	f, err := divide(1, 2)
	if err != nil {
		fmt.Println("Erro man")
		return
	}
	fmt.Println(f)
}
