package main

import "fmt"

func main() {
	var i interface{} = 1

	switch i.(type) {
	case int:
		fmt.Println("int")
		break
		fmt.Println("HELLO!!")
	case float64:
		fmt.Println("float64")
	case string:
		fmt.Println("string")
	case [3]int:
		fmt.Println("i is [3]int")
	default:
		fmt.Println("another value")
	}
}
