package main

import (
	"fmt"
	"sync"
	"time"
)

var a = sync.WaitGroup{}

func main() {
	/*
		create a channel and send message from one goroutine to another through it
		strings := make(chan string)
		a.Add(2)
		go func() {
			i := <-strings
			fmt.Println("Message from other goroutine:", i)
			a.Done()
		}()
		go func() {
			message := "Hey Mercedes"
			strings <- message
			message = "Make me a coffee please"
			a.Done()
		}()
		a.Wait()
	*/

	/*
		2 - 1taker and 5 senders. here is error

		delivery := make(chan string)
		go func() {
			message := <-delivery
			fmt.Println(message)
			a.Done()
		}()
		for i := 0; i < 5; i++ {
			a.Add(2)
			go func() {
				delivery <- "Hey Mercedes"
				a.Done()
			}()
		}
		a.Wait()*/

	/*3 set in parameter receiver or sender channel

	courier := make(chan string)
	a.Add(2)
	go func(courier <-chan string) {
		message := <-courier
		fmt.Println("Hello baby", message)
		a.Done()
	}(courier)
	go func(courier chan<- string) {
		courier <- "Mercedes"
		a.Done()
	}(courier)
	a.Wait()
	*/

	/*4

			-  for-each  with CHAN
			-  close CHAN

	ch := make(chan string, 50)
	a.Add(2)
	go func(ch <-chan string) {
		for message := range ch { //here is no index - only value
			fmt.Println(message)
		}
		a.Done()
	}(ch)


	go func(ch chan<- string) {
		ch <- "Boss in"
		ch <- "Bentley"
		close(ch) // if we close the channel then we can not reuse it again
		a.Done()
	}(ch)
	a.Wait()
	*/

	/*
		5 -
			manual for-each of CHAN

		delivery := make(chan string, 50)
		a.Add(2)
		go func(ch <-chan string) {
			for {
				if message, ok := <-ch; ok {
					fmt.Println(message)
				} else {
					break
				}
			}
			a.Done()

		}(delivery)

		go func(ch chan<- string) {
			ch <- "Hello"
			ch <- "Boss in bentley"
			close(ch)
			a.Done()

		}(delivery)
		a.Wait()*/

	/*6 close channel with DEFER()
	go logger()
	defer func() {// can be closed with defer function where we close the channel after main goroutine ends
		close(delivery)
	}()
	delivery <- messageLog{
		time:    time.Now(),
		severin: info,
		message: "Hey Mercedes",
	}
	delivery <- messageLog{
		time:    time.Now().Add(1 * time.Hour),
		severin: error1,
		message: "Hey Mercedes",
	}
	time.Sleep(1000 * time.Millisecond)
	*/

	/* 7 SELECT statement with CHAN */
	go logger()
	delivery <- messageLog{
		time:    time.Now(),
		severin: error1,
		message: "How can i help you",
	}

	delivery <- messageLog{
		time:    time.Now(),
		severin: info,
		message: "Hey Mercedes",
	}
	closeChan <- false
	time.Sleep(100 * time.Millisecond)
}

var delivery = make(chan messageLog, 50)

var closeChan = make(chan bool)

const (
	info   = "INFO"
	warn   = "WARNING"
	error1 = "ERROR"
)

type messageLog struct {
	time    time.Time
	severin string
	message string
}

func logger() {
	for {
		select {
		case message := <-delivery:
			fmt.Printf("%v - [%v]%v\n", message.time.Format("2006-01-02T15:04:01"), message.severin, message.message)
		case <-closeChan:
			fmt.Printf("CLOSE ME BABE\n")
			break
		}
	}
	//for entry := range delivery {
	//	fmt.Printf("%v - [%v]%v\n", entry.time.Format("2006-01-02T15:04:01"), entry.severin, entry.message)
	//}
}
