package main

func main() {

	/*
		1 simple interface with implementations
		var w Writer = TerminalWriter{}
		w.writeMeBaby([]byte("Hello baby!"))*/

	/*
		2  just create instance, convert to pointer and call methods on it

		intMyBaby := Increment(0)
		var inc GermanIncrementer = &intMyBaby
		for i := 0; i < 10; i++ {
			fmt.Println(inc.incrementMeBaby())
		}*/

	/*3
	bp := NewBufferedPlusWriterCloser()
	bp.Write([]byte("Hello baby here is boss in bentley"))
	bp.Close()
	*/

	/*5 type conversion
	var closer WriterCloser = NewBufferedPlusWriterCloser()
	closer.Write([]byte("Mercedes-Benz S-class coupe"))
	closer.Close()
	writerCloser, ok := closer.(*BufferedPlusWriterCloser) //casting
	if ok {
		fmt.Println(writerCloser)
	} else {

		fmt.Println("Conversion failed")
	}
	*/
	/*6 empty interface

	var emptyInterface interface{} = NewBufferedPlusWriterCloser()
	if writerClose, ok := emptyInterface.(WriterCloser); ok {
		writerClose.Write([]byte("Hello boss! 001"))
		writerClose.Close()
	}
	reader, ok := emptyInterface.(io.Reader)
	if ok {
		fmt.Println(reader)
	} else {
		fmt.Println("Conversion failed")
	}*/

	/*7
	var i interface{} = false
	switch i.(type) {
	case int:
		fmt.Println("i is an integer")
	case string:
		fmt.Println("i is an string")
	default:
		fmt.Println("who knows what it is")
	}
	*/
	/*8
	 */

}

/*
1 interface describes data. Not holds data

type Writer interface {
	writeMeBaby([]byte) (int, error)
}

type TerminalWriter struct {
}

func (tw TerminalWriter) writeMeBaby(dataBaby []byte) (int, error) {
	data, err := fmt.Println(string(dataBaby))
	return data, err
}
*/

/*

2 any type can have methods associated with an interface

type GermanIncrementer interface {
	incrementMeBaby() int
}

type Increment int

func (ic *Increment) incrementMeBaby() int {
	*ic++
	return int(*ic)
}
*/

/* 3-7
type Writer interface {

	Write([]byte) (int, error)
}

type Closer interface {
	Close() error
}

type WriterCloser interface {
	Writer
	Closer
}

type Str struct {
}
type BufferedPlusWriterCloser struct {
	buffer *bytes.Buffer
}

func (bpwc BufferedPlusWriterCloser) Write(bi []byte) (int, error) {
	write, err := bpwc.buffer.Write(bi)
	if err != nil {
		return 0, err
	}

	v := make([]byte, 8)
	for bpwc.buffer.Len() > 8 {
		_, err := bpwc.buffer.Read(v)
		if err != nil {
			return 0, err
		}
		fmt.Println(string(v))
		if err != nil {
			return 0, err
		}
	}
	return write, nil
}

func (bpwc BufferedPlusWriterCloser) Close() error {
	for bpwc.buffer.Len() > 0 {
		data := bpwc.buffer.Next(8)
		_, err := fmt.Println(string(data))
		if err != nil {
			return err
		}
	}
	return nil
}

func NewBufferedPlusWriterCloser() *BufferedPlusWriterCloser {
	return &BufferedPlusWriterCloser{
		buffer: bytes.NewBuffer([]byte{}),
	}*/
