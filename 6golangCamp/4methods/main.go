package main

import "fmt"

type Car struct {
	name string
	year int
}

func main() {
	car := Car{
		name: "Mercedes-Benz",
		year: 2021,
	}
	ints := []int{1, 2, 3}
	car.drive(ints...)

}

// method is the same as function but we call it from any type
func (car Car) drive(a ...int) {
	//There is same rules in pointer for the type. Because it is struct
	// here will be passed new instance which will be crashed after stack complete
	fmt.Println(car.name, "is good. And its year is", car.year)
}
