package main

import (
	"fmt"
	"runtime"
	"sync"
)

var wg = sync.WaitGroup{} //save to be used by different goroutines

var mut = sync.RWMutex{}

var counter = 0

func main() {
	/*
	   1 example with one goroutine and wait

	   	message := "Hey Mercedes"
	   	wg.Add(1)
	   	go func(message string) {
	   		sayMercedes(message)
	   		wg.Done()
	   	}(message)
	   	message = "Hey Mercedes-Benz"
	   	wg.Wait()
	*/

	/* 2

	 To keep synchronization need to take lock outside of methods, but it will break parallelism
	for i := 0; i < 10; i++ {
		wg.Add(2)
		mut.Lock()

		go increment()
		time.Sleep(3 * time.Millisecond)
		mut.RLock()

		go sayMercedes()
	}
	wg.Wait()
	*/
	/*3 GOMAXPROCS */
	numCPUs := runtime.NumCPU()
	fmt.Printf("Hello %s", numCPUs) //nolint:govet
	runtime.GOMAXPROCS(numCPUs)
	fmt.Printf("Threads: %v\n", runtime.GOMAXPROCS(91))
}

func increment() {
	counter++ //shared variable
	mut.Unlock()
	wg.Done()
}

func sayMercedes() {
	fmt.Println("Hey Mercedes", counter)
	mut.RUnlock()
	wg.Done()
}
