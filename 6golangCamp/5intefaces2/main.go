package main

import "fmt"

func main() {
	var closer WCBaby = &weiWriterCloser{}
	fmt.Println(closer)
}

type Writer interface {
	write(data []byte) (int, error)
}

type Closer interface {
	close() error
}

type WCBaby interface {
	Writer
	Closer
}

type weiWriterCloser struct {
}

func (w *weiWriterCloser) write(data []byte) (int, error) {
	return 0, nil
}
func (w weiWriterCloser) close() error {
	return nil
}
