package main

import "fmt"

func main() {
	fmt.Println("start")
	panicer()
	fmt.Println("end") // 3 will be executed because of recover
}

func panicer() {
	defer func() {
		if err := recover(); err != nil {
			fmt.Printf("err HELPED US: %v\n", err)
			//panic("Hello 2") //4  if we uncomment the code then we need to
			//handle with same recover on method outside
		}
	}()
	panic("end is here")  // 1 here will be go from the method
	fmt.Println("middle") // 2 will not be reached
}
