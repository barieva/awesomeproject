package main

import (
	"fmt"
	"io"
	"os"
)

var (
	d  = 2
	d2 = 2
	d3 = 2
	d4 = 2
)

func main1() {
	/*	a, b := 1, 0
		panic("hello")
		answer := a / b
		fmt.Println(answer)
	*/
	/*fmt.Println("Hello panic")
	panic("Hello from panic") //this panic not allow executing further
	fmt.Println("Alone message which never appeared")
	*/

	/*	http.HandleFunc("/", func(w http.ResponseWriter, r *http.Request) {
			w.Write([]byte("Hello Go!"))
		})
		err := http.ListenAndServe(":8082", nil)
		if err != nil {
			panic(err.Error())
		}
	*/
	/*//DEFER with PANIC
	fmt.Println("start")
	// defer cant be go down. It needs to be higher than panic to be executed
	defer fmt.Println("this was deferred (откложен)") // executes before panic()
	panic("AAA PANICC!! kek")                         // because maybe there is connection we need to close
	fmt.Println("end")
	*/
	fmt.Println(c())

}
func CopyFile(dstName, srcName string) (written int64, err error) {
	src, err := os.Open(srcName)
	defer src.Close()
	if err != nil {
		return
	}

	dst, err := os.Create(dstName)
	if err != nil {
		return
	}
	defer dst.Close()

	return io.Copy(dst, src)
}

func c() (i int) {
	defer func() { i++ }()
	return 1
}
