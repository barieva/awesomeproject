package main

import "fmt"

func main() {
	f()
	fmt.Println("Returned normally from f.") //6
}

func f() {
	defer func() {
		if r := recover(); r != nil {
			fmt.Println("Recovered in f", r) // 5
		}
	}()
	fmt.Println("Calling g.") // 1
	g(0)
	fmt.Println("Returned normally from g.") // will not execute due to error
}

func g(i int) {
	if i > 3 {
		fmt.Println("Panicking!") // 3
		panic(fmt.Sprintf("%v", i))
	}
	defer fmt.Println("Defer in g", i) // 4 LIFO  - i will start from 3 to 0
	fmt.Println("Printing in g", i)    // 2 - recursion is as looping
	g(i + 1)
}
