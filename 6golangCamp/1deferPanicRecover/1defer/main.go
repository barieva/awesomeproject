package main

import (
	"fmt"
	"io/ioutil"
	"log"
	"net/http"
)

func main() {

	/*DEFER - executes code after method complete and before it return*/
	/*DEFER - executes in LIFO order (last it first out) because one may be dependent on another one
	f.e. we close resources and better to close last opened resource not the first one*/
	defer fmt.Println("start")
	defer fmt.Println("middle")
	defer fmt.Println("end")

	/* Example with closing connection*/
	res, err := http.Get("http://www.google.com/robots.txt")
	if err != nil {
		log.Fatal(err)
	}
	defer res.Body.Close()
	robots, err := ioutil.ReadAll(res.Body)
	if err != nil {
		log.Fatal(err)
	}
	fmt.Printf("%s", robots)
	/*example with resetting value of variable*/

	a := "start"
	defer fmt.Println(a) // will be START, because program saves value which was set into method
	a = "end"

	// DEFER - gets only function call.
}
