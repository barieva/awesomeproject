package main

import "fmt"

func main() {
	strings := [3]string{"boss", "in", "bentley"}
	fmt.Println(&strings[0]) //takes 10 bytes between pointers
	fmt.Println(&strings[1]) // it allows easily go through them

	// Pointer with objects
	//mb := &Car{name: "Mercedes-Benz"}
	var car *Car
	fmt.Println(car)
	mb := new(Car)
	//(*mb).name = "Mercedes-Benz" | is same as below
	mb.name = "Mercedes-Benz" // syntactic sugar
	fmt.Println(mb)

	// Pointer with map
	var cars = map[string]string{"Mercedes-Benz": "S-coupe", "Audi": "Q7"}
	sameCopy := cars
	fmt.Println(cars, sameCopy)
	cars["Audi"] = "Q7 sport"
	fmt.Println(cars, sameCopy)

}

type Car struct {
	name string
}
