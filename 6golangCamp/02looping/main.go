package main

func main() {
	/* 1 ->

	for i, j := 0, 0; i < 5; i, j = i+1, j+1 {
			fmt.Println(i,j)
		}
	*/
	/*	2 ->

		i := 0
			for i < 5 {
				fmt.Println(i)
				if i%2 == 0 {
					i /= 2
				} else {
					i = 2*i + 1
				}
			}
	*/
	/*
		3 ->
		Loop:
		for i := 0; i <= 3; i++ {
			for j := 1; j <= 3; j++ {
				fmt.Println(i * j)
				if i*j >= 3 {
					break Loop // add label and go out not one but two loops
				}
			}
		}
	*/

	/* 4 -> */
	/*	s := "Hello world!"
		for i, v := range s {
			fmt.Println(i, string(v))
		}

		map1 := map[string]int{"Mercedes-Benz": 39250016}
		for _, v := range map1 {
			fmt.Println(v)
		}*/

}
