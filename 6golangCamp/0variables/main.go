package main

import (
	"fmt"
	"reflect"
)

type car struct {
	name string `required max:"100"` //web application. Called as TAG `TAG _INFO`
	year int
}

func main() {
	t := reflect.TypeOf(car{})
	field, _ := t.FieldByName("name")
	fmt.Println(field.Tag)
}
