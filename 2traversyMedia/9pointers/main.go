package main

import (
	"fmt"
	"time"
)

func main() {
	var time1 time.Time
	fmt.Println(time1.IsZero())
	fmt.Println(time1.Year())
	println(time.Now().After(time1))

	/*	layout := time.Now().Format(time.RFC3339)
		println(layout)
		var str2 time.Time= "2020-10-14T09:26:38Z"

		var deliveryDates []string = nil
		for a, b := range deliveryDates {
			fmt.Println(a, b)
		}

		//t, err := time.Parse("2006-01-02T15:04:05Z", `str)

		if err != nil {
			fmt.Println(err)
		}
		fmt.Println(t.Day())
		fmt.Println(t.Month())

		m := map[int]string{
			10: "Января",
		}
		s := m[int(t.Month())]
		fmt.Println(s)
		//_, month, day := timeStamp.Date()
		//fmt.Println(day)
		//fmt.Println(month)
		///*	println(time.Now().Date())
		day, month := getDayAndMonth("SDfsdf")
		fmt.Println(day, month)

		println(getTitle("2020-10-14T09:26:38Z"))
	*/
}

func getTitle(deliveryTime string) string {
	return "Довезем до" + getDeliveryTimeTitle(deliveryTime)
}

func getDeliveryTimeTitle(deliveryTime string) string {
	day, month := getDayAndMonth(deliveryTime)
	if day == -1 {
		return ""
	}
	return fmt.Sprintf("%d %s", day, convertMonth(month))
}

func getDayAndMonth(deliveryTime string) (int, time.Month) {
	parsedTime, err := time.Parse("2006-01-02T15:04:05Z", deliveryTime)
	if err != nil {
		return -1, time.January
	}
	return parsedTime.Day(), parsedTime.Month()
}

func convertMonth(month time.Month) string {
	months := map[int]string{
		1:  "января",
		2:  "февраля",
		3:  "марта",
		4:  "апреля",
		5:  "мая",
		6:  "июня",
		7:  "июля",
		8:  "августа",
		9:  "сентября",
		10: "октября",
		11: "ноября",
		12: "декабря",
	}
	return months[int(month)]
}
