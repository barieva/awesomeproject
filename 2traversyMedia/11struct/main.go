package main

import (
	"fmt"
	"strconv"
)

func main() {
	// explicitly declare fields
	car := Car{
		name:    "Bmw",
		price:   21003,
		country: "Germany",
	}
	fmt.Println(car)
	// implicitly declare fields
	car2 := Car{
		"Bmw",
		21003,
		"Germany",
	}

	fmt.Println(car2)
	fmt.Println(car2.name)
	car2.name = "Audi"
	fmt.Println(car2.name)

	// Methods are divided to two groups
	// Value receivers  - do calculation
	// and
	// Pointer receivers - do change state

	fmt.Println(car.turnOnByValueReceiver())
	fmt.Println(car.name)
	car.changeNameByPointer()
	fmt.Println(car.name)

	car.turnOnByValueReceiver()
	fmt.Println(car)
	car.getPrice(2020)
	fmt.Println(car)
	car.getPrice(2021)
	fmt.Println(car)
}

// Methods are divided to two groups

// Value receivers  - do calculation
func (car Car) turnOnByValueReceiver() string {
	return car.name + " turned on. Price of the car is " + strconv.FormatInt(car.price, 10)
}

// and
//Pointer receivers - do change state
func (car *Car) changeNameByPointer() {
	car.name = "Maserati"
}

func (car *Car) getPrice(year int) {
	if year < 2021 {
		return
	}
	car.price = 54293
}

type Car struct {
	name    string
	price   int64
	country string
}
