package main

import "fmt"

func main() {
	prices := []int{12313, 123423, 23645, 234234}
	for index, value := range prices {
		fmt.Printf("Index %d has price %d\n", index, value)
	}
	for _, value := range prices {
		fmt.Printf("price %d\n", value)
	}

	for index := range prices {
		fmt.Printf("index %d\n", index)
	}

	// add prices together
	var totalPrice int
	for _, price := range prices {
		totalPrice += price
	}
	fmt.Println("Total price is", totalPrice)

	// range with map
	cars := make(map[string]int)
	cars["Bentley"] = 120312
	cars["Audi"] = 53123

	for key, value := range cars {
		fmt.Printf("%s: %d\n", key, value)
	}

	for _, value := range cars {
		fmt.Println("Price is", value)
	}
}
