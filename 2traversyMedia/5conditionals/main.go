package main

import "fmt"

func main() {
	//todo if block
	/*x := 10
	y := 10

	if x <= y {
		fmt.Printf("%d is less than or equal to %d", x, y)
	} else {
		fmt.Printf("%d is less than %d", y, x)
	}*/

	/*color := "dark red"

	if color == "red" {
		fmt.Println("color is red")
	} else if color == "dark red" {
		fmt.Println("color is dark red")
	} else {
		fmt.Println("Make color gray")
	}*/

	// switch
	color := "black"

	switch color {
	case "black":
		fmt.Println("color is black")
	case "blue":
		fmt.Println("color is blue")
	default:
		fmt.Println("color is not blue or red")
	}
}
