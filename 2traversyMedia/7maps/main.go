package main

import "fmt"

func main() {
	//Define map
	cars := map[string]int{}
	cars2 := make(map[string]int)

	cars["Bentley"] = 120312
	cars["Audi"] = 53123
	fmt.Println(cars["Bentley"])
	fmt.Println(len(cars2))

	// delete map element

	delete(cars, "Audi")
	fmt.Println(cars)

	// declare map and assing value
	stores := map[string]int{
		"pulkovo": 123,
		"avtovo":  44,
		"cityPro": 421,
	}

	fmt.Println(stores)
}
