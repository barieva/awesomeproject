package main

import "fmt"

var name string = "Bmw"

func main() {
	var price int32 = 40123
	const cool = true
	carName, carPrice := "Audi", 112334

	fmt.Println(name, price)
	fmt.Println(carName, carPrice)
	fmt.Printf("%T\n", price) // %T represent type
}
