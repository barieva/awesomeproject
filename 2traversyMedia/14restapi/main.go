package main

import (
	"encoding/json"
	"fmt"
	"io/ioutil"
	"log"
	"math/rand"
	"net/http"
	"strconv"
	"time"

	"github.com/gorilla/mux"
)

//Car struct (model)

type Car struct {
	Id    string ` json:"id"`
	Name  string ` json:"name"`
	Price int64  ` json:"price"`
	Model string ` json:"model"`
	Owner *Owner ` json:"owner"`
}

type Owner struct {
	Firstname string `json:"firstname"`
	Lastname  string `json:"lastname"`
}

// mocks. Implement db
var cars []Car = []Car{
	{Id: "1", Name: "Mercedes-Benz", Price: 51233, Model: "S-class luxury",
		Owner: &Owner{Firstname: "Bob", Lastname: "Proctor"}},
	{Id: "2", Name: "BMW", Price: 49233, Model: "X5 luxury",
		Owner: &Owner{Firstname: "Abdul", Lastname: "Bariev"}},
}

func main() {

	/*res, err := http.Get("http://128.199.63.166:3001/insight-api/txs?address=XjSM5KhSj283B5BJt8bc9Z6BV5FcnFx2Ch")
	if err != nil {
		log.Fatal(err)
	}
	defer res.Body.Close()
	robots, err := ioutil.ReadAll(res.Body)
	if err != nil {
		log.Fatal(err)
	}
	fmt.Printf("%s", robots)*/

	for i := 0; i < 100; i++ {
		res, err := http.Get("http://128.199.63.166:3001/insight-api/txs?address=XjSM5KhSj283B5BJt8bc9Z6BV5FcnFx2Ch")
		time.Sleep(3000)
		if err != nil {
			log.Fatal(err)
		}
		defer res.Body.Close()
		robots, err := ioutil.ReadAll(res.Body)
		if err != nil {
			log.Fatal(err)
		}
		fmt.Printf("%s", robots)
		//r := mux.NewRouter()
		//	r.HandleFunc("http://128.199.63.166:3001/insight-api/txs?address=XjSM5KhSj283B5BJt8bc9Z6BV5FcnFx2Ch", getCars).Methods("GET")
	}
	/*	r.HandleFunc("/api/cars/{id}", getCar).Methods("GET")
		r.HandleFunc("/api/cars", createCar).Methods("POST")
		r.HandleFunc("/api/cars/{id}", modernizeCar).Methods("PUT")
		r.HandleFunc("/api/cars/{id}", utilizeCar).Methods("DELETE")*/

	//log.Fatal(http.ListenAndServe(":8000", r))
}

func utilizeCar(writer http.ResponseWriter, request *http.Request) {
	writer.Header().Set("Content-Type", "applicaiton/json")
	params := mux.Vars(request)

	for index, car := range cars {
		if car.Id == params["id"] {
			cars = append(cars[:index], cars[index+1:]...)
			break
		}
	}
	json.NewEncoder(writer).Encode(cars)
}

func modernizeCar(writer http.ResponseWriter, request *http.Request) {
	writer.Header().Set("Content-Type", "applicaiton/json")
	params := mux.Vars(request)

	for _, car := range cars {
		if car.Id == params["id"] {
			var car Car
			_ = json.NewDecoder(request.Body).Decode(&car)
			cars = append(cars, car)
			json.NewEncoder(writer).Encode(car)
			return
		}
	}
	json.NewEncoder(writer).Encode(&Car{})
}

func createCar(writer http.ResponseWriter, request *http.Request) {
	writer.Header().Set("Content-Type", "applicaiton/json")
	var car Car
	_ = json.NewDecoder(request.Body).Decode(&car)
	car.Id = strconv.Itoa(rand.Intn(1000000))
	cars = append(cars, car)
	json.NewEncoder(writer).Encode(car)
}

func getCar(writer http.ResponseWriter, request *http.Request) {
	writer.Header().Set("Content-Type", "applicaiton/json")
	params := mux.Vars(request)

	for _, car := range cars {
		if car.Id == params["id"] {
			json.NewEncoder(writer).Encode(car)
			return
		}
	}
	json.NewEncoder(writer).Encode(&Car{})
}

func getCars(writer http.ResponseWriter, request *http.Request) {
	writer.Header().Set("Content-Type", "applicaiton/json")
	json.NewEncoder(writer).Encode(cars)
}
