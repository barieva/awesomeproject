package main

import "fmt"

func incrementor() func(int) int {
	counter := 0
	return func(x int) int {
		counter += x
		return counter
	}
}
func main() {
	number := incrementor()

	for i := 0; i < 10; i++ {
		fmt.Println(number(i))
	}

	fmt.Println("Final number is:", number(0))
	fmt.Println("New number is:", incrementor()(1))
}
