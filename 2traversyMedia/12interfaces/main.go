package main

import "fmt"

type Car interface {
	moveFastHorses() int
}

type Mercedes struct {
	model     string
	mercHorse int
}

func (merc Mercedes) moveFastHorses() int {
	return merc.mercHorse + 10
}

type Bmw struct {
	model    string
	bmwHorse int
}

func (bmw Bmw) moveFastHorses() int {
	return bmw.bmwHorse + 4
}

func getMaxHorses(car Car) int {
	return car.moveFastHorses()
}
func main() {
	mercedes := Mercedes{
		model:     "S-coupe",
		mercHorse: 349,
	}

	bmw := Bmw{
		model:    "x5",
		bmwHorse: 350,
	}
	fmt.Println(mercedes.model, "moves fast horses", getMaxHorses(mercedes), "km/h")
	fmt.Println(bmw.model, "moves fast horses", getMaxHorses(bmw), "km/h")
}
