package main

import "fmt"

func main() {
	/*// declare
	var cars [2]string
	// assign
	cars[0] = "Audi"
	cars[1] = "Bmw"
	fmt.Println(cars)
	fmt.Println(cars[1])
	*/
	// declare and assign, slice
	var cars2 []string = []string{"Audi", "Bmw", "Bentley"}
	fmt.Println(len(cars2))
	fmt.Println(cars2[0:1]) // 0 - from , 1 - to not include

}
