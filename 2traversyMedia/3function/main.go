package main

import "fmt"

func welcome(name string) string {
	return "Hello " + name
}

func getSum(first int, second int) int {
	return first + second
}

func main() {

	fmt.Println(welcome("Mercedes-Benz"))
	fmt.Println(getSum(1, 2))
}
