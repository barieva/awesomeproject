package main

import "fmt"

func main() {
	/*
		//Simple for loop, as in java but without braces
			counter := 0

			for counter < 4 {
				fmt.Println(counter)
				counter++
			}

		for i := 0; i < 4; i++ {
			fmt.Println(i)
		}
	*/
	names := []string{"Gle", "Gls", "S"} //slice
	/*
		for i := 0; i < len(names); i++ {
			fmt.Println(names[i])
		}
	*/
	for index, value := range names {
		fmt.Printf("We are at index %v"+
			" and value is %v \n", index, value)
	}

	// If we dont wana use index then just replace it with underscore
	for _, value := range names {
		fmt.Printf("value is %v \n", value)
		value = "MERCEDES-BENZ"
	}
	fmt.Println(names)
}
