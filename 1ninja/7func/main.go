package main

import (
	"fmt"
	"math"
)

// function creation
func welcomeInCar(name string) int {
	fmt.Println("Mercedes-Benz welcomes you", name)
	return 1
}

func thankYou(name string) int {
	fmt.Println("Mercedes-Benz thanks", name)
	return 0
}

// here we can create function and pass function as argument.
//  In java we need to create functional interface and implement method
// In Go we just pass method which has same parameters and return values
func cycleNames(n []string, f func(string) int) {
	for _, value := range n {
		f(value)
	}
}

// Return value
func circleArea(r float64) float64 {
	return math.Pi * r * r
}

func main() {
	welcomeInCar("Bob")
	welcomeInCar("John")
	thankYou("John")

	cycleNames([]string{"bob", "john", "giorgio", "vitaly"}, welcomeInCar)
	cycleNames([]string{"bob", "john", "giorgio", "vitaly"}, thankYou)

	a1 := circleArea(10.5)
	a2 := circleArea(15)
	fmt.Println(a1, a2)
	fmt.Printf("circle 1 is %0.3f and \n file 2 is %0.5f", a1, a2)

}
