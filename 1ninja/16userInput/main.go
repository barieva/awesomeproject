package main

import (
	"bufio"
	"fmt"
	"os"
	"strconv"
	"strings"
)

type carStore struct {
	name string
	cars []car
}

type car struct {
	name       string
	price      float64
	indicators map[string]int
}

func newCarStore(name string) carStore {
	return carStore{
		name: name,
		cars: []car{},
	}
}

func newCar(name string) car {
	return car{
		name:       name,
		price:      0,
		indicators: map[string]int{},
	}
}

func main() {
	//car := createCar()
	carStore := newCarStore("AutoCompany")
	promptOption(&carStore)
}

func getInput(prompt string, reader *bufio.Reader) (string, error) {
	// here we read input from client
	// after new line we will read everything user passed
	// \n = enter
	fmt.Println(prompt)
	readString, err := reader.ReadString('\n')
	return strings.TrimSpace(readString), err
}

// prompt - подсказка
func createCar() car {
	// we get reader which can read input from console
	reader := bufio.NewReader(os.Stdin) // Stdin == standard input
	name, _ := getInput("Create a new car name: ", reader)
	c := newCar(name)
	fmt.Println("Created new car", c.name)
	return c
}

func promptOption(carStore *carStore) {
	reader := bufio.NewReader(os.Stdin)
	option, _ := getInput("Choose option ("+
		"a - add car, s - cave car)", reader)
	switch option {
	case "a":
		name, _ := getInput("Type name", reader)
		priceString, _ := getInput("Type price", reader)
		price, err := strconv.ParseFloat(priceString, 64)
		if err != nil {
			fmt.Println("The price must be a number")
			promptOption(carStore)
		}
		car := newCar(name)
		car.price = price
		carStore.cars = append(carStore.cars, car)
		fmt.Println("car added", name, price)
		promptOption(carStore)
	case "s":
		fmt.Println("you saved cars", carStore)
	default:
		fmt.Println("Chosen non existent option try again")
		promptOption(carStore)
	}
}
