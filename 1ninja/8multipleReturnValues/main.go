package main

import (
	"fmt"
	"strings"
)

func getCarInitials(carName string) (string, string) {
	carNames := strings.Split(carName, " ")

	if len(carNames) == 1 && carNames[0] == "" {
		return "_", "_"
	}
	var carInitials []string
	for _, value := range carNames {
		carInitials = append(carInitials, strings.ToUpper(value[:1]))
	}
	if len(carInitials) > 1 {
		return carInitials[0], carInitials[1]
	}
	return carInitials[0], "_"
}

func main() {
	initials, s := getCarInitials("Mercedes-Benz Bmw")
	carInitials, s2 := getCarInitials("Mercedes-Benz")
	s3, s4 := getCarInitials("")
	fmt.Println(s3, s4)
	fmt.Println(initials, s)
	fmt.Println(carInitials, s2)
}
