package main

import "fmt"

// create interface with keyword - "interface"
// it just declares methods signatures
// method signature - name, parameters, return values
type car interface {
	turnOn(name string) string
}

// then we create structs with simple 1variables. Notice we cant create functions inside of struct
type bmw struct {
	name        string
	doorsOpened bool
	turnedOn    bool
}

type audi struct {
	name        string
	doorsOpened bool
	turnedOn    bool
}

// here we create function which belongs to bmw
func (bmw bmw) turnOn(name1 string) string {
	fmt.Println("Turn on car on BMW style. By the way there is name:", name1)
	return "BMW TOP"
}

// and function with same signature which belongs to audi
func (audi audi) turnOn(name string) string {
	fmt.Println("Turn on car on Audi style. NAME is:", name)
	return "Audi TOP"
}

// as we noticed we have interface and structs which have same functions.
// But every struct has different body

// And here is polymorphism
// We put interface inside the function and
// simply use the function with different structs
func dealWithCar(car car) {
	car.turnOn("Bentley")
}

func main() {
	// there is no implement keywords.
	// just need to follow one rule:
	// Struct has to have the "same functions with the same signatures" to be considered as interface
	audi := audi{name: "audi", turnedOn: false, doorsOpened: false}
	bmw := bmw{name: "audi", turnedOn: false, doorsOpened: false}
	dealWithCar(audi)
	dealWithCar(bmw)
	// There is no need to use "implement" keyword as in java. Just have the same functions and that is it
}
