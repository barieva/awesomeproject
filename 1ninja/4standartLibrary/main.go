package main

import (
	"fmt"
	"sort"
)

func main() {
	/*	mesg := "Hello world "

		// strings Package:

		//Contains method
		fmt.Println(strings.Contains(mesg, "rld"))

		// ReplaceAll method returns new string with replaced value.
		//   Old string gona be the same
		fmt.Println(strings.ReplaceAll(mesg, "world", "Mercedes-Benz"))
		fmt.Println(mesg)

		// ToUpper - returns new string camel case
		fmt.Println(strings.ToUpper(mesg))

		// Index - returns number where specific substring starts in string
		fmt.Println(strings.Index(mesg, "ld"))

		//Split (разбивать) - returns array with elements, separated by substring (substrings gona be deleted)
		fmt.Println(strings.Split(mesg, "l"))
	*/

	// ---------------------------------------------

	// sort package

	prices := []int{45000, 23100, 12300, 45001, 44512} // slice of integer

	// sort.Ints - sorts existent array in ascending order
	sort.Ints(prices)
	fmt.Println(prices)

	// SearchInts -  returns first existent index of value
	index := sort.SearchInts(prices, 23100)
	fmt.Println(index)

	// SearchInts - if value not exists then we will get len(prices) == last index in array + 1
	noIndex := sort.SearchInts(prices, 99999)
	fmt.Println(noIndex)

	// sort.Strings - sorts existent array by default alphabetically
	names := []string{"Mercedes-Benz", "Bmw", "Audi"}
	sort.Strings(names)
	fmt.Println(names)
	// SearchStrings searches in array by substring
	fmt.Println(sort.SearchStrings(names, "Mercedes"))
}
