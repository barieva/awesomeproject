package main

import "fmt"

func main() {
	age := 12
	name := "Bob Proctor"
	/*fmt.Print("hello, ")
	fmt.Print("world")

	fmt.Println("MY AGE IS!!", age, "and my name issss",name)
	*/
	//fmt.Printf(formatted string)

	fmt.Printf("my name isss %q and ageeege %q \n", name, age) // adds quotes only to string, other types will be with default '\f' or '#'
	fmt.Printf("my name isss %v and ageeege %v \n", name, age) // make 1variable as string
	fmt.Printf("age type isss %T \n", age)                     // gets TYPE of 1variable
	fmt.Printf("age type isss %f\n", 123.1)                    // prints float number
	fmt.Printf("age type isss %0.2f\n", 123.316)               // prints float number and specified 8range after dot rounding it

	//Sprintf() is the same as Printf but

	newStringFromSprintfGood := fmt.Sprintf("hello %v", age)
	fmt.Print(newStringFromSprintfGood)
}
