package main

import "fmt"

func main() {
	var cars = map[string]int{
		"Bmw":     432234,
		"Audi":    432234,
		"Bentley": 432234,
	}

	fmt.Println(cars)
	fmt.Println("no existent car", cars["Mb"])
	fmt.Println("existent car", cars["Bmw"])

	cars["Audi"] = 0
	delete(cars, "Bmw")
	fmt.Println(cars)
}
