package main

import (
	"fmt"
	"os"
)

func main() {
	fileName := "car"
	data := []byte("Hello Mercedes-Benz")
	err := os.WriteFile("17saveInFile/"+fileName+".txt", data, 0644)
	if err != nil {
		panic(err)
	}
	fmt.Println("File saved")
}
