package main

import "fmt"

type car struct {
	name string
}

// here we create copy of instance every time.
//In high load app it can be overhead
func (c car) changeCopyVariableName() {
	// c - here is just copy
	c.name = "BMW"
}

// here we create copy of pointer every time. It saves memory
func (c *car) changeName() {
	// c - here is pointer which implicitly casts to original 1variable
	c.name = "BMW"
}

func main() {
	germanCar := car{
		name: "Audi",
	}
	germanCar.changeCopyVariableName()
	fmt.Println(germanCar.name)
	germanCar.changeName()
	fmt.Println(germanCar.name)
}
