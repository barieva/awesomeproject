package main

import "fmt"

func main() {
	price := 45000
	fmt.Println(price == 45000)
	fmt.Println(price != 15000)
	fmt.Println(price <= 466000)
	fmt.Println(price >= 46000)
	// if else
	if price < 30000 {
		fmt.Println("Price is less than 30000")
	} else if price < 40000 {
		fmt.Println("Price is less than 40000")
	} else {
		fmt.Println("Price is more than 40000")
	}
	names := []string{"Gle", "Gls", "S", "Glc", "E"} //slice

	// continue and break
	for index, value := range names {
		if index == 2 {
			fmt.Println("skip at index", index)
			continue
		}
		if index > 3 {
			fmt.Println("stop at index", index)
			break
		}
		fmt.Printf("INDEX %v. VALUE %v \n", index, value)
	}
}
