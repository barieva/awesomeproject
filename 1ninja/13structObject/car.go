package main

// Time to speak about objects
// In goLang they called as -> STRUCTs

// We declare struct (class) with "type" and "struct" keywords
type car1 struct {
	// here we declare 1variable name and its type
	name string
	// we do not need equals sign or comma
	price int
	// just variableName and type
	indicators map[string]int
}

// here we declare function which will do work for object creation
func newCar(name string) car1 {
	return car1{
		name:       name,
		price:      0,
		indicators: map[string]int{},
	}
}
