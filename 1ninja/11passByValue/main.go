package main

import "fmt"

func main() {
	// First group -> (ints, floats, bools, strings), arrays, structs
	// these types will not update their value if we pass them exactly
	// into method argument

	// We can't update 1variable value passing it into method
	var name = "Audi" // name -> "Audi"
	// pass NAME into method which will create a copy
	updateCarName(name)
	// print original 1variable value
	fmt.Println(name)

	//======================

	// We can call method which will return value and assign it
	// to 1variable
	name = updateCarNameWithReturn(name)
	// Then it will have modified value
	fmt.Println(name)

	// Second group -> slices, maps, functions
	// their values are changeable when we put them into function
	// as argument
	var namesByPrices = map[string]int{}
	updatePriceByName(namesByPrices)
	fmt.Println(namesByPrices)
}

func updateCarName(name string) {
	name = "Bentley"
}

func updateCarNameWithReturn(name string) string {
	return "Bentley"
}

func updatePriceByName(namesByPrices map[string]int) {
	namesByPrices["Audi"] = 12134
	namesByPrices = map[string]int{
		"Bmw": 12345,
	}
}
