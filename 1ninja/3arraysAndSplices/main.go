package main

import "fmt"

func main() {
	var mercedeses = [3]int{30, 1, 0}
	names := [4]string{"Bob", "Dan", "Vitaly"}
	names[1] = "GIORGIO ARMANI"
	fmt.Println(mercedeses, len(mercedeses))
	fmt.Println(names, len(names))
	// slices (use arrays under the hood)
	var scores = []int{100, 43, 1233}
	scores[2] = 25
	scores1 := append(scores, 1123)

	fmt.Println(scores, len(scores))
	fmt.Println(scores1, len(scores))

	// slice ranges
	rangeOne := names[2:4]
	rangeTwo := names[1:]
	rangeThree := names[:1]
	fmt.Println(rangeOne)
	fmt.Println(rangeTwo)
	fmt.Println(rangeThree)

	rangeOne = append(rangeOne, "BOZ IN BNTL")
	fmt.Println(rangeOne)

}
