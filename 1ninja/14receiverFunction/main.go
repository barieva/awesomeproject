package main

import "fmt"

func main() {
	store := newCarStore("PULCOVO")
	cars := store.formatCars()
	fmt.Println(cars)
}
