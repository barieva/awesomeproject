package main

import "fmt"

// in previous module we learned how to create Struct(class)
// and methods which will return new instance to us
type carStore struct {
	name string
	cars map[string]float64
}

// The function above belongs to no one. It can be called without creating instance
// i have associations with such methods as with static ones
func newCarStore(name string) carStore {
	return carStore{
		name: name,
		cars: map[string]float64{
			"Mercedes-Benz": 1234.43,
			"Audi":          12355.3443,
			"BMW":           123,
		},
	}
}

// The method above is static, it can be called without instance
// But what if i want to associate method with concrete instance?
// Here is  RECEIVER function
// It is the same function but with struct type with it

func (carStore) calculatePrice(equipment string) int {
	// after "func" keyword we have added struct type in parentheses (carStore)
	// now the method can be called only from object with carStore type
	switch equipment {
	case "Basic":
		return 51200
	case "Standard":
		return 70423
	case "Luxury":
		return 123004
	default:
		return 63300
	}
}

// did you mention store var? It is used to get some data from
// instance which called the method. so cool.
// pay attention we cant change struct state(values) inside of
// method. We have only copy of struct
func (store carStore) formatCars() string {
	format := "Prices breakdown: \n"
	var total float64 = 0

	// list items
	for name, price := range store.cars {
		format += fmt.Sprintf("%-25v:...%v \n", name, price)
		total += price
	}

	// %-25v is so interesting thing
	// it tells what 1variable need to be 25 characters
	// and if it will be less then please add spaces to
	// the end of it (because in our case there is minus sign)
	// for it to be 25 characters in total
	format += fmt.Sprintf("%-25v...%v", "total:", total)
	return format
}
