package main

import "fmt"

func main() {

	// In previous module we learned what we cant change value of
	// 1variable passing it into method as argument

	// I mean Variable - primitive(string,numbers), array or struct -
	// is unchanged in terms of passing it into method

	// But some time we want to pass 1variable and change its value
	// inside of method.
	// f.e. i want to pass struct and change its value. Why not.

	// For such cases was introduced POINTER feature
	// we can have:
	// 1variable = value
	mercedes := "53230"
	fmt.Println(mercedes)
	// and
	// pointer = &1variable
	pointerToVariable := &mercedes
	fmt.Println(pointerToVariable) // memory location

	// pointer is something what points to 1variable location

	// from pointer we can get value
	// value = *pointer
	mercedesPrice := *pointerToVariable
	fmt.Println(mercedesPrice)

	//----------------------------

	// It can help us with passing pointer into method as argument
	// getting exact value which we cant change.
	// and value will not be copied value
	bmw := "52344"
	fmt.Println("Now bwm price is", bmw)
	pricePointer := &bmw
	changePrice(pricePointer)
	fmt.Println("But we change it and now price is", bmw)
}

func changePrice(price *string) {
	*price = "999999"
}
